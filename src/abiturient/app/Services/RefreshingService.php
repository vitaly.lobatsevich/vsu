<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

/**
 * This service is used to synchronize local catalogs with ELUNI
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 * @property string $eluniLogin login of the user who has access to the ELUNI "apiAbiturient" API
 * @property string $eluniPassword password of this user
 * @property string $eluniBaseUrl
 * @property string $cookieHeader cookies for authorization received from eluni
 */
abstract class RefreshingService
{
    private $eluniLogin;
    private $eluniPassword;
    protected $eluniBaseUrl;
    protected $cookieHeader;

    /**
     * Constructor makes HTTP request for authorization to ELUNI service and saves aithorization cookie
     * It retrieves credentials from .env file
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     */
    public function __construct()
    {
        $this->eluniBaseUrl = env('ELUNI_BASE_URL', 'https://eluni.vsu.by');
        $this->eluniLogin = env('ELUNI_LOGIN', null);
        $this->eluniPassword = env('ELUNI_PASSWORD', null);

        foreach (Http::retry(
            env('REFRESHING_NUMBER_OF_ATTEMPTS', 3),
            env('REFRESHING_TIME_BETWEEN_ATTEMPTS', 100)
        )->timeout(env('REFRESHING_REQUEST_TIMEOUT', 3))->withOptions([
            'verify' => false,
        ])->asForm()->post("$this->eluniBaseUrl/a_main_page/login", [
            'login' => $this->eluniLogin,
            'password' => $this->eluniPassword,
        ])->cookies()->toArray() as $authorizationCookie)
            $this->cookieHeader .= $authorizationCookie['Name'] . '=' . $authorizationCookie['Value'] . ';';
    }

    /**
     * Gets collection from ELUNI
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param string $url
     * @return array
     */
    public function get($url)
    {
        return Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Cookie' => $this->cookieHeader,
        ])->get("$this->eluniBaseUrl/apiAbiturient/$url")
            ->json()['result'];
    }

    /**
     * This method gets catalog collection from ELUNI and updates local collection
     * The implementation method should update existing elements, add missing ones, but not remove those present here but not present in ELUNI
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     */
    public abstract function refresh($onComplete = null);
}
