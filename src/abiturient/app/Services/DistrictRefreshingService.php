<?php

namespace App\Services;

use App\Models\District;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class DistrictRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('districts') as $district) {
            $dbDistrict = District::find($district['id']);
            if ($dbDistrict === null)
                District::create($district);
            else
                $dbDistrict->update($district);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}
