<?php

namespace App\Services;

use App\Models\SpecialtyTrial;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyTrialRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        SpecialtyTrial::truncate();
        foreach ($this->get('specialties_trials') as $specialtyTrial) {
            if (
                SpecialtyTrial::where('specialty_id', $specialtyTrial['specialtyId'])
                    ->where('trial_id', $specialtyTrial['trialId'])
                    ->get()
                    ->isEmpty()
            ) {
                SpecialtyTrial::create($specialtyTrial);
            }
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}
