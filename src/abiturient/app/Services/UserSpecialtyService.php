<?php

namespace App\Services;

use App\Models\UserSpecialty;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserSpecialtyService
{
    /**
     * Returns UserSpecialty realation by user and specialty id
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $userId
     * @param int $specialtyId
     * @return array|null
     */
    public function getByUserIdAndSpecialtyId($userId, $specialtyId)
    {
        $userSpecialty = UserSpecialty::where('user_id', $userId)
            ->where('specialty_id', $specialtyId)
            ->get();
        if ($userSpecialty->isEmpty())
            return null;
        return $userSpecialty->first();
    }

    /**
     * Returns all users specialties by user id
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $userId
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getByUserId($userId)
    {
        return UserSpecialty::where('user_id', $userId)
            ->get();
    }

    /**
     * Saves new user-specialty relation
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $userId
     * @param int $specialtyId
     * @return array|App\Models\UserSpecialty
     */
    public function save($userId, $specialtyId)
    {
        $userSpecialty = $this->getByUserIdAndSpecialtyId(
            $userId,
            $specialtyId
        );
        if (!isset($userSpecialty)) {
            $userSpecialty = UserSpecialty::create([
                'userId' => $userId,
                'specialtyId' => $specialtyId,
            ]);
        }
        return $userSpecialty;
    }

    /**
     * Deletes user-specialty relation by user and specialty id
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $userId
     * @param int $specialtyId
     */
    public function deleteByUserIdAndSpecialtyId($userId, $specialtyId)
    {
        UserSpecialty::where('user_id', $userId)
            ->where('specialty_id', $specialtyId)
            ->delete();
    }
}
