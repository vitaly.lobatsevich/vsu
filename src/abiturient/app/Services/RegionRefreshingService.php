<?php

namespace App\Services;

use App\Models\Region;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RegionRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('regions') as $region) {
            $dbRegion = Region::find($region['id']);
            if ($dbRegion === null)
                Region::create($region);
            else
                $dbRegion->update($region);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}
