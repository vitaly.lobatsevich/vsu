<?php

namespace App\Services;

use App\Models\Trial;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class TrialRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('trials') as $trial) {
            $dbTrial = Trial::find($trial['id']);
            if ($dbTrial === null)
                Trial::create($trial);
            else
                $dbTrial->update($trial);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}
