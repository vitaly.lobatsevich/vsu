<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Personal document type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class PersonalDocumentType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}
