<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AppBreak extends AppModel
{
    use HasFactory;

    protected $table = 'breaks';

    protected $fillable = [
        'start',
        'end',
        'admissionId',
    ];

    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
        'admissionId' => 'integer',
    ];

    public function admission()
    {
        return $this->belongsTo(Admission::class);
    }
}
