<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Educational institution model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitution extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'educationalInstitutionTypeId',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
