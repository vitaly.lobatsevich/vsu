<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Educational institution type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}
