<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * District model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class District extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'regionId',
    ];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function localities()
    {
        return $this->hasMany(Locality::class);
    }
}
