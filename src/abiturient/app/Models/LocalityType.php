<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Locality type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'shortName',
    ];
}
