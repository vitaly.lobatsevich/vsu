<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Country model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Country extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];

    /**
     * This method makes eager retrieving region models
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     */
    public function regions()
    {
        return $this->hasMany(Region::class);
    }
}
