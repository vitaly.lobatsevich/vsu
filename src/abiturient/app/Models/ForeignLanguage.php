<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Foreign language model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class ForeignLanguage extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}
