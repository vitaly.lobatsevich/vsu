<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Street type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class StreetType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'shortName',
    ];
}
