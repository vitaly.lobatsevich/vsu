<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Locality model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Locality extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'districtId',
        'localityTypeId',
    ];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function birthedUsers()
    {
        return $this->hasMany(User::class, 'place_of_birth');
    }

    public function registerdUsers()
    {
        return $this->hasMany(User::class, 'place_of_registration');
    }
}
