<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationForm extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'shortName',
    ];
}
