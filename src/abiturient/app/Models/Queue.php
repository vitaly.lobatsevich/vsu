<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Queue extends AppModel
{
    use HasFactory;

    protected $table = 'queue';

    protected $fillable = [
        'id',
        'userId',
        'admissionId',
        'deskId',
        'dateTime',
    ];

    protected $casts = [
        'dateTime' => 'date',
    ];

    public function desk()
    {
        return $this->belongsTo(Desk::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
