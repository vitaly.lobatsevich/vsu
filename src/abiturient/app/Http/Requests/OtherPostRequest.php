<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save other info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class OtherPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isBrsmMember' => 'required|boolean',
            'isDormitoryNeeded' => 'required|boolean',
            'isMilitaryDuty' => 'required|boolean',
            'recruitingOfficeId' => 'nullable|exists:recruiting_offices,id',
            'workPlace' => 'nullable|string',
            'seniority' => 'required|numeric|min:0',
        ];
    }
}
