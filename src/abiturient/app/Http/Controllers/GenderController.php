<?php

namespace App\Http\Controllers;

use App\Models\Gender;

/**
 * Gender collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class GenderController extends Controller
{
    /**
     * Returns gender by id or all genders if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\Gender|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id !== null ? Gender::find($id) : Gender::all();
    }
}
