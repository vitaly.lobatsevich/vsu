<?php

namespace App\Http\Controllers;

use App\Models\ForeignLanguage;

/**
 * Foreign language collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class ForeignLanguageController extends Controller
{
    /**
     * Returns foreign language by id or all languages if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\ForeignLanguage|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? ForeignLanguage::all() : ForeignLanguage::find($id);
    }
}
