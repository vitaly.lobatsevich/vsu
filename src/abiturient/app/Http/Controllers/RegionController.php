<?php

namespace App\Http\Controllers;

use App\Models\Region;

/**
 * Region collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RegionController extends Controller
{
    /**
     * Returns region by id or all regions if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int $id
     * @return App\Models\Region|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? Region::all() : Region::find($id);
    }
}
