<?php

namespace App\Http\Controllers;

use App\Models\Trial;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class TrialController extends Controller
{
    /**
     * Returns trial by id or all trials if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\Trial|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? Trial::with([
            'trialType',
        ])->get() : Trial::with([
            'trialType',
        ])->find($id);
    }
}
