<?php

namespace App\Http\Controllers;

use App\Models\EducationalInstitutionType;

/**
 * Educational intitution type collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class EducationalInstitutionTypeController extends Controller
{
    /**
     * Returns educational institution type by id or all types if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\EducationalInstitutionType|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? EducationalInstitutionType::all() : EducationalInstitutionType::find($id);
    }
}
