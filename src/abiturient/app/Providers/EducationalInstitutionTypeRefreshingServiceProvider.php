<?php

namespace App\Providers;

use App\Services\EducationalInstitutionTypeRefreshingService;
use Illuminate\Support\ServiceProvider;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionTypeRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EducationalInstitutionTypeRefreshingService::class, function ($app) {
            return new EducationalInstitutionTypeRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
