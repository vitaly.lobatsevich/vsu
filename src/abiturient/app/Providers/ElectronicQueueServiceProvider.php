<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ElectronicQueueService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class ElectronicQueueServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ElectronicQueueService::class, function ($app) {
            return new ElectronicQueueService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
