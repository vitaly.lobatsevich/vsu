<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\FacultyRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class FacultyRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FacultyRefreshingService::class, function ($app) {
            return new FacultyRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
