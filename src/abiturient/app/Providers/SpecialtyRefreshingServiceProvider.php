<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\SpecialtyRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SpecialtyRefreshingService::class, function ($app) {
            return new SpecialtyRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
