<?php

namespace App\Providers;

use App\Services\EducationalInstitutionRefreshingService;
use Illuminate\Support\ServiceProvider;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EducationalInstitutionRefreshingService::class, function ($app) {
            return new EducationalInstitutionRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
