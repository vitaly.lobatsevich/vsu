<?php

namespace App\Providers;

use App\Services\RegionRefreshingService;
use Illuminate\Support\ServiceProvider;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RegionRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RegionRefreshingService::class, function ($app) {
            return new RegionRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
