<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\SpecialtyTrialRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyTrialRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SpecialtyTrialRefreshingService::class, function ($app) {
            return new SpecialtyTrialRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
