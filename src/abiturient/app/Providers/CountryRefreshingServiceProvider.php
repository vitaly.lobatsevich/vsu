<?php

namespace App\Providers;

use App\Services\CountryRefreshingService;
use Illuminate\Support\ServiceProvider;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CountryRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CountryRefreshingService::class, function ($app) {
            return new CountryRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
