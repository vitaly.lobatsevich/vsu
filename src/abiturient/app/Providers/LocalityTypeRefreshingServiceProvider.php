<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\LocalityTypeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityTypeRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LocalityTypeRefreshingService::class, function ($app) {
            return new LocalityTypeRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
