<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EducationForm;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EducationForm::create([
            'name' => 'Дневная форма',
            'shortName' => 'ДФПО',
        ]);
        EducationForm::create([
            'name' => 'Заочная форма',
            'shortName' => 'ЗФПО',
        ]);
        EducationForm::create([
            'name' => 'Заочная сокращённая форма',
            'shortName' => 'ЗФПО (сокр.)',
        ]);
    }
}
