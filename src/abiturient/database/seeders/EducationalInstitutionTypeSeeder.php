<?php

namespace Database\Seeders;

use App\Services\EducationalInstitutionTypeRefreshingService;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionTypeSeeder extends Seeder
{
    private $educationalInstitutionTypeRefreshingService;

    public function __construct(
        EducationalInstitutionTypeRefreshingService $educationalInstitutionTypeRefreshingService
    ) {
        $this->educationalInstitutionTypeRefreshingService = $educationalInstitutionTypeRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->educationalInstitutionTypeRefreshingService->refresh();
    }
}
