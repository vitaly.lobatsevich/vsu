<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\LocalityTypeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityTypeSeeder extends Seeder
{
    protected $localityTypeRefreshingService;

    public function __construct(LocalityTypeRefreshingService $localityTypeRefreshingService)
    {
        $this->localityTypeRefreshingService = $localityTypeRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->localityTypeRefreshingService->refresh();
    }
}
