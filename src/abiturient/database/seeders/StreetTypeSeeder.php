<?php

namespace Database\Seeders;

use App\Models\StreetType;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class StreetTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StreetType::create([
            'name' => 'Улица',
            'shortName' => 'ул.',
        ]);
        StreetType::create([
            'name' => 'Проспект',
            'shortName' => 'пр-т',
        ]);
        StreetType::create([
            'name' => 'Переулок',
            'shortName' => 'пер.',
        ]);
        StreetType::create([
            'name' => 'Проезд',
            'shortName' => 'п-д',
        ]);
    }
}
