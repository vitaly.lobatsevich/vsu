<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TrialType;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class TrialTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrialType::create([
            'name' => 'Централизованное тестирование',
            'shortName' => 'ЦТ',
        ]);
        TrialType::create([
            'name' => 'Экзамен',
            'shortName' => 'Экз.',
        ]);
    }
}
