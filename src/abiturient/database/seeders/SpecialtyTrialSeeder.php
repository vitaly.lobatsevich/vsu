<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Services\SpecialtyTrialRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyTrialSeeder extends Seeder
{
    private $specialtyTrialRefreshingService;

    public function __construct(SpecialtyTrialRefreshingService $specialtyTrialRefreshingService)
    {
        $this->specialtyTrialRefreshingService = $specialtyTrialRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->specialtyTrialRefreshingService->refresh();
    }
}
