<?php

namespace Database\Seeders;

use App\Models\PersonalDocumentType;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class PersonalDocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonalDocumentType::create([
            'name' => 'паспорт',
        ]);
        PersonalDocumentType::create([
            'name' => 'вид на жительство',
        ]);
        PersonalDocumentType::create([
            'name' => 'паспорт иностранного гражданина',
        ]);
    }
}
