<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breaks', function (Blueprint $table) {
            $table->dateTime('start')
                ->nullable(false);
            $table->dateTime('end')
                ->nullable(false);
            $table->foreignId('admission_id')
                ->nullable(false)
                ->constrained();

            $table->timestamps();

            $table->primary([
                'start',
                'end',
                'admission_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breaks');
    }
}
