<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateSpecialtiesTrialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialties_trials', function (Blueprint $table) {
            $table->foreignId('specialty_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('trial_id')
                ->nullable(false)
                ->constrained();

            $table->timestamps();

            $table->primary([
                'specialty_id',
                'trial_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialties_trials');
    }
}
