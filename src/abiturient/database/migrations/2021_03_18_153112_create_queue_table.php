<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queue', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('admission_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('desk_id')
                ->nullable(false)
                ->constrained();
            $table->dateTime('date_time')
                ->nullable(false);

            $table->timestamps();

            $table->unique([
                'user_id',
                'admission_id',
            ]);
            $table->unique([
                'desk_id',
                'date_time',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue');
    }
}
