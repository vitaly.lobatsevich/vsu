<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\EducationalDocumentTypeController;
use App\Http\Controllers\EducationalInstitutionController;
use App\Http\Controllers\EducationalInstitutionTypeController;
use App\Http\Controllers\ForeignLanguageController;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\HonorController;
use App\Http\Controllers\LocalityController;
use App\Http\Controllers\LocalityTypeController;
use App\Http\Controllers\PersonalDocumentTypeController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\SportCategoryController;
use App\Http\Controllers\SportTypeController;
use App\Http\Controllers\StreetTypeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserSportTypeSportCategoryController;
use App\Http\Controllers\RecruitingOfficeController;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\EducationFormController;
use App\Http\Controllers\SpecialtyController;
use App\Http\Controllers\ElectronicQueueController;
use App\Http\Controllers\AdmissionController;
use App\Http\Controllers\UserSpecialtyController;
use App\Http\Controllers\TrialController;
use App\Http\Controllers\MonitoringController;
use App\Http\Controllers\SpecialtyTrialController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * AuthController
 */

Route::post('/register', [
    AuthController::class,
    'register'
]);

Route::post('/email/verification-notification', [
    AuthController::class,
    'resend'
])->middleware(['auth:sanctum', 'throttle:6,1'])
    ->name('verification.send');

Route::post('/change-password', [
    AuthController::class,
    'changePassword'
])->middleware('auth:sanctum');

/*
 * UserController
 */
Route::middleware('auth:sanctum')->get('/users/{id?}', [
    UserController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->get('/users/me', [
    UserController::class,
    'getMe'
]);

Route::middleware('auth:sanctum')->post('/users/basic-info', [
    UserController::class,
    'postBasicInfo'
]);

Route::middleware('auth:sanctum')->post('/users/passport-info', [
    UserController::class,
    'postPassportInfo'
]);

Route::middleware('auth:sanctum')->post('/users/address', [
    UserController::class,
    'postAddress'
]);

Route::middleware('auth:sanctum')->post('/users/education', [
    UserController::class,
    'postEducation'
]);

Route::middleware('auth:sanctum')->post('/users/family', [
    UserController::class,
    'postFamily'
]);

Route::middleware('auth:sanctum')->post('/users/other', [
    UserController::class,
    'postOther'
]);

Route::post('/email/check', [
    UserController::class,
    'checkEmail'
]);

/*
 * GenderController
 */
Route::get('/genders/{id?}', [
    GenderController::class,
    'get'
])->whereNumber('id');

/*
 * CountryController
 */
Route::get('/countries/{id?}', [
    CountryController::class,
    'get'
])->whereNumber('id');

/*
 * PersonalDocumentTypeController
 */
Route::get('/personal-document-types/{id?}', [
    PersonalDocumentTypeController::class,
    'get'
])->whereNumber('id');

/*
 * RegionController
 */
Route::get('/regions/{id?}', [
    RegionController::class,
    'get'
])->whereNumber('id');

/*
 * DistrictController
 */
Route::get('/districts/{id?}', [
    DistrictController::class,
    'get'
])->whereNumber('id');

/*
 * LocalityTypeController
 */
Route::get('/locality-types/{id?}', [
    LocalityTypeController::class,
    'get'
])->whereNumber('id');

/*
 * LocalityController
 */
Route::get('/localities/{id?}', [
    LocalityController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->post('/localities/refresh', [
    LocalityController::class,
    'refresh'
]);

/*
 * StreetTypeController
 */
Route::get('/street-types/{id?}', [
    StreetTypeController::class,
    'get'
])->whereNumber('id');

/*
 * EducationDocumentTypeController
 */
Route::get('/educational-document-types/{id?}', [
    EducationalDocumentTypeController::class,
    'get'
])->whereNumber('id');

/*
 * EducationalInstitutionTypeController
 */
Route::get('/educational-institution-types/{id?}', [
    EducationalInstitutionTypeController::class,
    'get'
])->whereNumber('id');

/*
 * EducationalInstitutionController
 */
Route::get('/educational-institutions/{id?}', [
    EducationalInstitutionController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->post('/educational-institutions/refresh', [
    EducationalInstitutionController::class,
    'refresh'
]);

/*
 * ForeignLanguageController
 */
Route::get('/foreign-languages/{id?}', [
    ForeignLanguageController::class,
    'get'
])->whereNumber('id');

/*
 * HonorController
 */
Route::get('/honors/{id?}', [
    HonorController::class,
    'get'
])->whereNumber('id');

/*
 * SportTypeController
 */
Route::get('/sport-types/{id?}', [
    SportTypeController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->post('/sport-types/refresh', [
    SportTypeController::class,
    'refresh'
]);

/*
 * SportCategoryController
 */
Route::get('/sport-categories/{id?}', [
    SportCategoryController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->post('/sport-categories/refresh', [
    SportCategoryController::class,
    'refresh'
]);

/*
 * UserSportTypeSportCategoryController
 */
Route::middleware('auth:sanctum')->get('/users-sport-types-sport-categories', [
    UserSportTypeSportCategoryController::class,
    'get'
]);

Route::middleware('auth:sanctum')->post('/users-sport-types-sport-categories', [
    UserSportTypeSportCategoryController::class,
    'post'
]);

Route::middleware('auth:sanctum')->delete('/users-sport-types-sport-categories/{sportTypeId}', [
    UserSportTypeSportCategoryController::class,
    'delete'
])->whereNumber('sportTypeId');

/*
 * RecruitingOfficeController
 */
Route::get('/recruiting-offices/{id?}', [
    RecruitingOfficeController::class,
    'get'
])->whereNumber('id');

Route::middleware('auth:sanctum')->post('/recruiting-offices/refresh', [
    RecruitingOfficeController::class,
    'refresh'
]);

/*
 * FacultyController
 */
Route::get('/faculties/{id?}', [
    FacultyController::class,
    'get'
])->whereNumber('id');

/*
 * EducationFormController
 */
Route::get('/education-forms/{id?}', [
    EducationFormController::class,
    'get'
])->whereNumber('id');

/*
 * SpecialtyController
 */
Route::get('/specialties/{id}', [
    SpecialtyController::class,
    'get'
])->whereNumber('id');

Route::get('/specialties', [
    SpecialtyController::class,
    'getCurrentYear'
]);

/*
 * ElectronicQueueController
 */
Route::middleware('auth:sanctum')->get('/electronic-queue/is-has-access', [
    ElectronicQueueController::class,
    'isHasAccess'
]);

Route::middleware('auth:sanctum')->post('/electronic-queue/me', [
    ElectronicQueueController::class,
    'postMe'
]);

Route::middleware('auth:sanctum')->delete('/electronic-queue/me', [
    ElectronicQueueController::class,
    'deleteMe'
]);

Route::post('/electronic-queue/magistrants', [
    ElectronicQueueController::class,
    'postMagistrant'
]);

/*
 * AdmissionController
 */
Route::get('/admissions/current', [
    AdmissionController::class,
    'getAbiturientCurrent'
]);

Route::get('/admissions/current-magistrant', [
    AdmissionController::class,
    'getMagistrantCurrent'
]);

/*
 * UserSpecialtyController
 */
Route::middleware('auth:sanctum')->get('/users-specialties/me', [
    UserSpecialtyController::class,
    'getMe'
]);

Route::middleware('auth:sanctum')->post('/users-specialties', [
    UserSpecialtyController::class,
    'post'
]);

Route::middleware('auth:sanctum')->delete('/users-specialties/{id}', [
    UserSpecialtyController::class,
    'delete'
])->whereNumber('id');

/*
 * TrialController
 */
Route::get('/trials/{id?}', [
    TrialController::class,
    'get',
])->whereNumber('id');

/*
 * MonitoringController
 */
Route::middleware('auth:sanctum')->get('/monitoring/{deskId}', [
    MonitoringController::class,
    'getAbiturientScheduleByDeskId',
])->whereNumber('deskId');

Route::middleware('auth:sanctum')->get('/magistrant-monitoring/{deskId}', [
    MonitoringController::class,
    'getMagistrantScheduleByDeskId'
])->whereNumber('deskId');

/*
 * SpecialtyTrialController
 */
Route::middleware('auth:sanctum')->post('/specialties-trials/refresh', [
    SpecialtyTrialController::class,
    'refresh'
]);
