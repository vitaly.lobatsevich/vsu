import React from 'react';

import strings from '../../strings';
import formatter from '../../formatter';
import {postcode} from '../../validators';
import Text from '../text';

const Postcode = ({
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    countryId = 0,
    afterChange = () => {},
}) => {
    const doSetValue = value => setValue(countryId !== 1 
        ? value.toUpperCase().replace(/[^A-Z0-9-]/g, formatter.translate)
        : value.replace(/[^0-9]/g, ''));

    return (
        <Text
            id="postcode"
            label={strings.postcode}
            value={value}
            setValue={doSetValue}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            placeholder={countryId !== 1 ? '' : strings.postcodePlaceholder}
            maxLength={countryId !== 1 ? 100 : 6}
            validators={countryId !== 1 ? [] : [postcode]}
            afterChange={afterChange}
        />
    );
};

export default Postcode;
