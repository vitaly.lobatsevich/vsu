import React, {useState, useEffect, useContext, useCallback} from 'react';
import Moment from 'react-moment';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';

import strings from '../../strings';
import {AppContext} from '../../contexts';
import {selectDbUser} from '../../redux/slices/db-user-slice';
import {selectAdmission} from '../../redux/slices/admission-slice';
import {selectAdmissionDownloaded} from '../../redux/slices/admission-downloaded-slice';
import {isAdmissionStarted} from '../private-route';
import Spinner from '../spinner';
import CalendarCurrentTime from '../electronic-queue/calendar-current-time';

const Progress = () => {
    const user = useSelector(selectDbUser);
    const admission = useSelector(selectAdmission);
    const isAdmissionDownloaded = useSelector(selectAdmissionDownloaded);
    const {isTimeSelected} = useContext(AppContext);

    const isCounted = useCallback(item => {
        switch (item) {
            case 'id':
            case 'email':
            case 'emailVerifiedAt':
            case 'createdAt':
            case 'updatedAt':

            case 'patronymic':
            case 'patronymicBy':
            case 'street':
            case 'housing':
            case 'apartment':
            case 'educationalInstitutionId':
            case 'motherPatronymic':
            case 'fatherPatronymic':
            case 'motherWorkPlace':
            case 'fatherWorkPlace':
            case 'motherPhone':
            case 'fatherPhone':
            case 'honorId':
            case 'numberOfSiblings':
            case 'workPlace':
            case 'seniority':

            case 'educationalInstitution':

            case 'isRuralEducationalInstitution':
            case 'isPedagogicalClass':
            case 'isPreUniversityTraining':
            case 'isMarried':
            case 'isBrsmMember':
            case 'isDormitoryNeeded':
            case 'isMilitaryDuty':

            case 'placeOfBirthLocality':
            case 'placeOfRegistrationLocality':
            case 'queue':
            case 'specialties':
                return false;
            case 'personalDocumentSeries':
            case 'personalDocumentNumber':
            case 'personalIdentificationNumber':
            case 'personalDocumentIssueDate':
            case 'personalDocumentValidity':
            case 'personalDocumentOrganization':
                return user.personalDocumentTypeId !== 3;
            case 'recruitingOfficeId':
                return user.isMilitaryDuty;
            default: return true;
        }
    }, [user]);

    const getProgress = useCallback(() => Math.round(
        Object
            .keys(user)
            .filter(isCounted)
            .filter(item => user[item] !== undefined && user[item] !== null && user[item] !== '' && user[item] !== 0).length
        * 100
        / Object
            .keys(user)
            .filter(isCounted).length
    ), [user, isCounted]);

    const [progress, setProgress] = useState(getProgress());

    const getColor = () => {
        if (progress >= 75)
            return 'bg-success';
        if (progress >= 50)
            return 'bg-warning';
        return 'bg-danger';
    };

    const getTitle = () => {
        if (!progress)
            return '';
        if (progress === 100)
            return <span className="text-success">{strings.progress100}</span>
        if (progress >= 75)
            return <span className="text-success">{strings.progress75}</span>
        if (progress >= 50)
            return <span className="text-warning">{strings.progress50}</span>
        return <span className="text-danger">{strings.progress0}</span>
    };

    useEffect(() => setProgress(getProgress()), [user, getProgress]);

    return (
        !isAdmissionDownloaded ?
        <div className="text-center">
            <Spinner />
        </div> :
        <div>
            <div className="row">
                    <div className="col">
                    <h4 align="center">
                        {strings.progress}
                    </h4>
                    <hr />
                    <div className="progress" style={{height: '25px'}}>
                        <div
                            className={`progress-bar progress-bar-striped progress-bar-animated ${getColor()}`}
                            role="progressbar"
                            aria-valuenow={progress}
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{width: `${progress}%`}}
                        >
                            {progress ? progress + '%' : ''}
                        </div>
                    </div>
                    <small>
                        {getTitle()}
                    </small>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col">
                    <span className="text-muted small">
                        {strings.registrationDescription}:
                    </span>
                </div>
            </div>
            <div className="row">
                {isTimeSelected() ?
                <div className="col">
                    <CalendarCurrentTime
                        isCancelShown={false}
                        isChangeShown={true}
                    />
                </div> :
                !isAdmissionStarted(admission) ?
                <div className="col">
                    <span className="text-warning">
                        {strings.registrationWillOpenFrom}
                        &nbsp;
                        <Moment format="DD.MM.YYYY">
                            {admission.startRegistration}
                        </Moment>
                    </span>
                </div> :
                <div
                    className="col"
                    title={getProgress() < 75 ? strings.registrationWillAvailable : ''}
                >
                    <Link
                        to="/electronic-queue"
                        className={
                            `btn ${getProgress() < 75 ? 'disabled' :
                                getProgress() === 100 ? 'btn-success' : 'btn-outline-success'} btn-lg btn-block mt-4`
                        }
                    >
                        {strings.signUp}
                    </Link>
                </div>
                }
            </div>
        </div>
    );
};

export default Progress;
