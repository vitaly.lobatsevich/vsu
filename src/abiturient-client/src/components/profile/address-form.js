import React, {useState, useContext} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import {postcode, validate as validatePostcode} from '../../validators';
import {AuthContext} from '../../contexts';
import {selectUser, setValue} from '../../redux/slices/user-slice';
import {selectConstants} from '../../redux/slices/constants-slice';
import AddressInput from './address-input';
import Alert from '../alert';
import TextInput from '../text-input';
import Postcode from './postcode';
import SubmitButton from '../submit-button';
import HelpButton from '../help-button';

const AddressForm = ({setIsChanged = () => {}}) => {
    const dispatch = useDispatch();
    const user = useSelector(selectUser);
    const constants = useSelector(selectConstants);
    const authContext = useContext(AuthContext);

    const [countryOfRegistrationId, setCountryOfRegistrationId] = useState(0);
    const [streetTypeId, setStreetTypeId] = useState(1);
    const [postcodeError, setPostcodeError] = useState('');
    const [isStreetValid, setIsStreetValid] = useState(false);
    const [isHouseValid, setIsHouseValid] = useState(false);
    const [isHousingValid, setIsHousingValid] = useState(false);
    const [isApartmentValid, setIsApartmentValid] = useState(false);
    const [isPostcodeValid, setIsPostcodeValid] = useState(false);
    const [success, setSuccess] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const clearValidation = () => {
        setPostcodeError('');

        setIsStreetValid(false);
        setIsHouseValid(false);
        setIsHousingValid(false);
        setIsApartmentValid(false);
        setIsPostcodeValid(false);
    };

    const validate = () => user.postcode !== '' ? validatePostcode(
        user.postcode,
        setPostcodeError,
        setIsPostcodeValid,
        [postcode]
    ) : true;

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/users/address', {
                placeOfBirth: formatter.foreignKey(user.placeOfBirth),
                placeOfRegistration: formatter.foreignKey(user.placeOfRegistration),
                street: formatter.str(user.street),
                house: formatter.str(user.house),
                housing: formatter.str(user.housing),
                apartment: formatter.str(user.apartment),
                postcode: user.postcode,
            }).then(() => {
                clearValidation();
                authContext.getMe();
                setIsChanged(false);
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <h5>
                        {strings.placeOfBirth}
                    </h5>
                    <AddressInput
                        id="placeOfBirth"
                        localityId={user.placeOfBirth}
                        setLocalityId={value => dispatch(setValue({key: 'placeOfBirth', value: value}))}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <h5>
                        {strings.placeOfRegistration}
                        <HelpButton content={strings.registrationHelp} />
                    </h5>
                    <AddressInput
                        id="placeOfRegistration"
                        localityId={user.placeOfRegistration}
                        setLocalityId={value => dispatch(setValue({key: 'placeOfRegistration', value: value}))}
                        setExternalCountryId={setCountryOfRegistrationId}
                        fixCountryId={1}
                        afterChange={() => setIsChanged(true)}
                    />
                    <div className="form-group">
                        <label htmlFor="street">
                            {strings.street}
                            <HelpButton content={strings.streetHelp} />
                        </label>
                        <div className="row">
                            <div className="col-4" style={{paddingRight: '0px'}}>
                                <select
                                    value={streetTypeId}
                                    onChange={({target}) => setStreetTypeId(parseInt(target.value))}
                                    className="custom-select"
                                >
                                    {constants.streetTypes.map(streetType =>
                                    <option key={streetType.id} value={streetType.id}>
                                        {streetType.name}
                                    </option>
                                    )}
                                </select>
                            </div>
                            <div className="col-8">
                                <TextInput
                                    id="street"
                                    value={user.street}
                                    setValue={value => dispatch(setValue({key: 'street', value: value}))}
                                    isValid={isStreetValid}
                                    setIsValid={setIsStreetValid}
                                    afterChange={() => setIsChanged(true)}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row">
                            <div className="col">
                                <label htmlFor="house">
                                    {strings.house}
                                </label>
                                <TextInput
                                    id="house"
                                    value={user.house}
                                    setValue={value => dispatch(setValue({key: 'house', value: value}))}
                                    isValid={isHouseValid}
                                    setIsValid={setIsHouseValid}
                                    afterChange={() => setIsChanged(true)}
                                />
                            </div>
                            <div className="col">
                                <label htmlFor="housing">
                                    {strings.housing}
                                </label>
                                <TextInput
                                    id="housing"
                                    value={user.housing}
                                    setValue={value => dispatch(setValue({key: 'housing', value: value}))}
                                    isValid={isHousingValid}
                                    setIsValid={setIsHousingValid}
                                    afterChange={() => setIsChanged(true)}
                                />
                            </div>
                            <div className="col">
                                <label htmlFor="apartment">
                                    {strings.apartment}
                                </label>
                                <TextInput
                                    id="apartment"
                                    value={user.apartment}
                                    setValue={value => dispatch(setValue({key: 'apartment', value: value}))}
                                    isValid={isApartmentValid}
                                    setIsValid={setIsApartmentValid}
                                    afterChange={() => setIsChanged(true)}
                                />
                            </div>
                        </div>
                    </div>
                    <Postcode
                        value={user.postcode}
                        validationError={postcodeError}
                        setValidationError={setPostcodeError}
                        setValue={value => dispatch(setValue({key: 'postcode', value: value}))}
                        isValid={isPostcodeValid}
                        setIsValid={setIsPostcodeValid}
                        countryId={countryOfRegistrationId}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Alert
                        classes="alert-success"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default AddressForm;
