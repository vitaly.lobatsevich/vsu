import React from 'react';

import Specialties from './specialties';
import SpecialtyPicker from '../specialty-picker';

const SpecialtyForm = () => (
    <div>
        <div className="card">
            <div className="card-body">
                <SpecialtyPicker />
            </div>
        </div>
        <div className="card mt-3">
            <div className="card-body">
                <Specialties />
            </div>
        </div>
    </div>
);

export default SpecialtyForm;
