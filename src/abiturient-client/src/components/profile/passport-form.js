import React, {useState, useContext} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import client from '../../client';
import strings from '../../strings';
import formatter from '../../formatter';
import {personalIdentificationNumber, validate as validateValue} from '../../validators';
import {AuthContext} from '../../contexts'
import {selectUser, setValue} from '../../redux/slices/user-slice';
import Passport, {validateSeries, validateNumber} from '../passport';
import Text from '../text';
import PassportDate, {validate as validatePassportDate} from './passport-date';
import Alert from '../alert';
import SubmitButton from '../submit-button';

const identificationNumberValidators = [personalIdentificationNumber];

const PassportForm = ({setIsChanged = () => {}}) => {
    const dispatch = useDispatch();
    const user = useSelector(selectUser);
    const authContext = useContext(AuthContext);

    const [seriesError, setSeriesError] = useState('');
    const [numberError, setNumberError] = useState('');
    const [identificationNumberError, setIdentificationNumberError] = useState('');
    const [issueDateError, setIssueDateError] = useState('');
    const [validityError, setValidityError] = useState('');
    const [isSeriesValid, setIsSeriesValid] = useState(false);
    const [isNumberValid, setIsNumberValid] = useState(false);
    const [isIdentificationNumberValid, setIsIdentificationNumberValid] = useState(false);
    const [isIssueDateValid, setIsIssueDateValid] = useState(false);
    const [isValidityValid, setIsValidityValid] = useState(false);
    const [isOrganizationValid, setIsOrganizationValid] = useState(false);
    const [success, setSuccess] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const validate = () => {
        const isPersonalDocumentSeriesValid =
            user.personalDocumentTypeId === 3 ||
            validateSeries(
                user.personalDocumentSeries,
                setSeriesError,
                setIsSeriesValid,
                user.personalDocumentTypeId
            );
        const isPersonalDocumentNumberValid =
            user.personalDocumentTypeId === 3 ||
            validateNumber(
                user.personalDocumentNumber,
                setNumberError,
                setIsNumberValid,
                user.personalDocumentTypeId
            );
        const isPersonalIdentificationNumberValid =
            user.personalIdentificationNumber === '' ||
            user.personalDocumentTypeId === 3 ||
            validateValue(
                user.personalIdentificationNumber,
                setIdentificationNumberError,
                setIsIdentificationNumberValid,
                identificationNumberValidators
            );
        const isPassportDateValid = 
            (!user.personalDocumentIssueDate && !user.personalDocumentValidity) || (
            validatePassportDate(
                user.personalDocumentIssueDate,
                user.personalDocumentValidity,
                setIssueDateError,
                setValidityError,
                setIsIssueDateValid,
                setIsValidityValid,
                user.personalDocumentTypeId
            ));

        return isPersonalDocumentSeriesValid
            && isPersonalDocumentNumberValid
            && isPersonalIdentificationNumberValid
            && isPassportDateValid;
    };

    const clearValidation = () => {
        setSeriesError('');
        setNumberError('');
        setIdentificationNumberError('');
        setIssueDateError('');
        setValidityError('');

        setIsSeriesValid(false);
        setIsNumberValid(false);
        setIsIdentificationNumberValid(false);
        setIsIssueDateValid(false);
        setIsValidityValid(false);
        setIsOrganizationValid(false);
    };

    const handleSubmit = event => {
        event.preventDefault();

        clearValidation();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/users/passport-info', {
                citizenship: formatter.foreignKey(user.citizenship),
                personalDocumentTypeId: formatter.foreignKey(user.personalDocumentTypeId),
                personalDocumentSeries: user.personalDocumentSeries,
                personalDocumentNumber: user.personalDocumentNumber,
                personalIdentificationNumber: user.personalIdentificationNumber,
                personalDocumentIssueDate: formatter.date(user.personalDocumentIssueDate),
                personalDocumentValidity: formatter.date(user.personalDocumentValidity),
                personalDocumentOrganization: formatter.str(user.personalDocumentOrganization),
            }).then(() => {
                clearValidation();
                authContext.getMe();
                setIsChanged(false);
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <Passport
                        citizenship={user.citizenship}
                        setCitizenship={value => dispatch(setValue({
                            key: 'citizenship', 
                            value: value
                        }))}
                        documentTypeId={user.personalDocumentTypeId}
                        setDocumentTypeId={value => dispatch(setValue({
                            key: 'personalDocumentTypeId', 
                            value: parseInt(value, 10)
                        }))}
                        series={user.personalDocumentSeries}
                        setSeries={value => dispatch(setValue({
                            key: 'personalDocumentSeries', 
                            value: value
                        }))}
                        number={user.personalDocumentNumber}
                        setNumber={value => dispatch(setValue({
                            key: 'personalDocumentNumber', 
                            value: value
                        }))}
                        seriesError={seriesError}
                        setSeriesError={setSeriesError}
                        numberError={numberError}
                        setNumberError={setNumberError}
                        isSeriesValid={isSeriesValid}
                        setIsSeriesValid={setIsSeriesValid}
                        isNumberValid={isNumberValid}
                        setIsNumberValid={setIsNumberValid}
                        afterChange={() => {setIsChanged(true)}}
                    />
                    <hr />
                    <Text
                        id="identificationNumber"
                        label={
                            <div>
                                {strings.identificationNumber}
                                {user.personalDocumentTypeId === 3 &&
                                <small className="text-muted"> - {strings.ifAvailable}</small>
                                }
                            </div>
                        }
                        value={user.personalIdentificationNumber}
                        setValue={value => dispatch(setValue({
                            key: 'personalIdentificationNumber',
                            value: value.toUpperCase().replace(/[^0-9A-Z]/g, formatter.translate)
                        }))}
                        validationError={identificationNumberError}
                        setValidationError={setIdentificationNumberError}
                        isValid={isIdentificationNumberValid}
                        setIsValid={setIsIdentificationNumberValid}
                        placeholder={user.personalDocumentTypeId !== 3 
                            ? strings.identificationNumberPlaceholder : ''}
                        validators={user.personalDocumentTypeId !== 3 
                            ? identificationNumberValidators : []}
                        maxLength={user.personalDocumentTypeId !== 3 ? 14 : 100}
                        help={strings.identificationNumberHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <PassportDate
                        issueDate={user.personalDocumentIssueDate}
                        setIssueDate={value => dispatch(setValue({
                            key: 'personalDocumentIssueDate',
                            value: value
                        }))}
                        validity={user.personalDocumentValidity}
                        setValidity={value => dispatch(setValue({
                            key: 'personalDocumentValidity',
                            value: value
                        }))}
                        issueDateError={issueDateError}
                        setIssueDateError={setIssueDateError}
                        validityError={validityError}
                        setValidityError={setValidityError}
                        isIssueDateValid={isIssueDateValid}
                        setIsIssueDateValid={setIsIssueDateValid}
                        isValidityValid={isValidityValid}
                        setIsValidityValid={setIsValidityValid}
                        documentTypeId={user.personalDocumentTypeId}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Text
                        id="organization"
                        label={strings.personalDocumentOrganization}
                        value={user.personalDocumentOrganization}
                        setValue={value => dispatch(setValue({
                            key: 'personalDocumentOrganization', 
                            value: value
                        }))}
                        isValid={isOrganizationValid}
                        setIsValid={setIsOrganizationValid}
                        help={strings.organizationHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Alert
                        classes="alert-success"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default PassportForm;
