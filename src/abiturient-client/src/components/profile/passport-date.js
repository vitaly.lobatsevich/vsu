import React from 'react';

import strings from '../../strings';
import {default as DateGroup} from '../date';

export const validate = (
    issueDate = null, 
    validity = null, 
    setIssueDateError = () => {}, 
    setValidityError = () => {}, 
    setIsIssueDateValid = () => {}, 
    setIsValidityValid = () => {},
    documentTypeId = 1
) => {
    setIssueDateError('');
    setValidityError('');
    setIsIssueDateValid(false);
    setIsValidityValid(false);

    if (documentTypeId !== 3) {
        if (!issueDate && validity) {
            setIssueDateError(strings.issueDateRequired);
            return false;
        }
        if (issueDate && !validity) {
            setValidityError(strings.validityRequired);
            return false;
        }
    }
    if (validity && validity.getTime() <= new Date().getTime()) {
        setValidityError(strings.passportExpired);
        return false;
    }
    if (issueDate && issueDate.getTime() >= new Date().getTime()) {
        setIssueDateError(strings.notIssued);
        return false;
    }
    if (issueDate && validity && issueDate.getTime() >= validity.getTime()) {
        setIssueDateError(strings.issueDateGreaterValidity);
        return false;
    }

    setIsIssueDateValid(true);
    setIsValidityValid(true);
    return true;
};

const PassportDate = ({
    issueDate,
    setIssueDate = () => {},
    validity,
    setValidity = () => {},
    issueDateError = '',
    setIssueDateError = () => {},
    validityError = '',
    setValidityError = () => {},
    isIssueDateValid = false,
    setIsIssueDateValid = () => {},
    isValidityValid = false,
    setIsValidityValid = () => {},
    documentTypeId = 1,
    afterChange = () => {},
}) => {
    return (
        <div>
            <DateGroup
                id="issueDate"
                label={
                    <div>
                        {strings.issueDate}
                        {documentTypeId === 3 &&
                        <small className="text-muted"> - {strings.ifAvailable}</small>
                        }
                    </div>
                }
                date={issueDate}
                setDate={setIssueDate}
                validationError={issueDateError}
                setValidationError={setIssueDateError}
                isValid={isIssueDateValid}
                setIsValid={setIsIssueDateValid}
                help={strings.issueDateHelp}
                afterChange={afterChange}
            />
            <DateGroup
                id="validity"
                label={
                    <div>
                        {strings.validity}
                        {documentTypeId === 3 &&
                        <small className="text-muted"> - {strings.ifAvailable}</small>
                        }
                    </div>
                }
                date={validity}
                setDate={setValidity}
                validationError={validityError}
                setValidationError={setValidityError}
                isValid={isValidityValid}
                setIsValid={setIsValidityValid}
                help={strings.validityHelp}
                afterChange={afterChange}
            />
        </div>
    );
};

export default PassportDate;
