import React from 'react';

import strings from '../strings';

import './loading-indicator-spinner.scss';

const LoadingIndicatorSpinner = ({isOnTop = true}) => (
    <div className={`loading-indicator ${isOnTop ? 'loading-indicator-top' : ''}`}>
        <div className="spinner-border text-primary">
            <span className="sr-only">
                {strings.loading}
            </span>
        </div>
    </div>
);

export default LoadingIndicatorSpinner;
