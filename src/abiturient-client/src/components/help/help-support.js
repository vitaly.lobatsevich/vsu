import React from 'react';

import strings from '../../strings';

const HelpSupport = () => (
    <div>
        <p>
            {strings.supportHelp1}
        </p>
        <p>
            {strings.supportHelp2}
        </p>
    </div>
);

export default HelpSupport;
