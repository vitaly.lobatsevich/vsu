import React from 'react';
import {useBeforeunload} from 'react-beforeunload';

import strings from '../strings';
import Layout from './layout';

const RegistrationLayout = ({form}) => {
    useBeforeunload(event => event.preventDefault());

    return (
        <Layout title={strings.titles.registration}>
            <div className="container">
                <div className="row">
                    <div className="col-sm-1 col-md-2 col-lg-3 col-xl-4" />
                    <div className="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-4">
                        <div className="card">
                            <div className="card-body">
                                {form}
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-1 col-md-2 col-lg-3 col-xl-4" />
                </div>
            </div>
        </Layout>
    );
};

export default RegistrationLayout;
