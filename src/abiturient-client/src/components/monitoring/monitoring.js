import React, {useState} from 'react';
import moment from 'moment';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import {selectAdmission} from '../../redux/slices/admission-slice';
import {selectAdmissionDownloaded} from '../../redux/slices/admission-downloaded-slice';
import {selectMagistrantAdmission} from '../../redux/slices/magistrant-admission-slice';
import {selectMagistrantAdmissionDownloaded} from '../../redux/slices/magistrant-admission-downloaded-slice';
import Layout from '../layout';
import Spinner from '../spinner';
import Radio from '../radio';
import Select from '../select';
import FormGroup from '../form-group';
import AbiturientList from './abiturient-list';
import MagistrantList from './magistrant-list';

const Monitoring = () => {
    const [level, setLevel] = useState(1);
    const [deskId, setDeskId] = useState(0);
    const [date, setDate] = useState(moment());
    const [isAllDates, setIsAllDates] = useState(false);

    const doSetLevel = level => {
        setDeskId(0);
        setLevel(level);
    };

    const admission = useSelector(selectAdmission);
    const isAdmissionDownloaded = useSelector(selectAdmissionDownloaded);
    const magistrantAdmission = useSelector(selectMagistrantAdmission);
    const isMagistrantAdmissionDownloaded = useSelector(selectMagistrantAdmissionDownloaded);

    const isLoading = () => !isAdmissionDownloaded || !isMagistrantAdmissionDownloaded;

    const getCurrentAdmission = () => {
        switch (level) {
            case 1:
                return admission;
            case 2:
                return magistrantAdmission;
            default:
                return null;
        }
    };

    const getDesks = () => getCurrentAdmission() ? getCurrentAdmission().desks : [];

    const getDeskOptions = () => [{
        value: 0,
        label: strings.all
    }, ...getDesks().map(desk => ({
        value: desk.id,
        label: desk.description,
    }))];

    const handleDateChange = ({target}) => setDate(moment(target.value));

    return (
        <Layout title={strings.titles.monitoring}>
            {isLoading() ?
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="text-center">
                            <Spinner />
                        </div>
                    </div>
                </div>
            </div> :
            <div className="container">
                <div className="row mb-3">
                    <div className="col">
                        <h3>
                            {strings.monitoringElectronicQueue}
                        </h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-6 col-lg-4 mb-3">
                        <div className="card">
                            <div className="card-body">
                                <Radio
                                    id="level"
                                    name="level"
                                    value={level}
                                    setValue={doSetLevel}
                                    values={[
                                        {
                                            id: 1,
                                            name: strings.abiturients
                                        },
                                        {
                                            id: 2,
                                            name: strings.magistrants
                                        },
                                    ]}
                                    isInline={true}
                                    label=""
                                />
                                <FormGroup
                                    id="deskId"
                                    label={strings.desk}
                                    input={
                                        <Select
                                            id="deskId"
                                            value={deskId}
                                            setValue={setDeskId}
                                            options={getDeskOptions()}
                                            withNotSelected={true}
                                        />
                                    }
                                />
                                <FormGroup
                                    id="date"
                                    label={strings.date}
                                    input={
                                        <input
                                            type="date"
                                            value={date.format('YYYY-MM-DD')}
                                            onChange={handleDateChange}
                                            className="form-control"
                                            disabled={isAllDates}
                                        />
                                    }
                                />
                                <div className="custom-control custom-checkbox">
                                    <input
                                        type="checkbox"
                                        checked={isAllDates}
                                        onChange={({target}) => setIsAllDates(target.checked)}
                                        id="isAllDates"
                                        className="custom-control-input"
                                    />
                                    <label htmlFor="isAllDates" className="custom-control-label">
                                        {strings.allDates}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-8">
                        <div className="card">
                            <div className="card-body">
                                {level === 1 ?
                                <AbiturientList deskId={deskId} date={date} isAllDates={isAllDates} /> :
                                <MagistrantList deskId={deskId} date={date} isAllDates={isAllDates} />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            }
        </Layout>
    );
};

export default Monitoring;
