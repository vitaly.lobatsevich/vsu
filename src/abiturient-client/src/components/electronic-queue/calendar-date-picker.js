import React from 'react';
import moment from 'moment';

import strings from '../../strings';
import FormGroup from '../form-group';
import Select from '../select';

import './calendar-date-picker.scss';

const CalendarDatePicker = ({
    year = parseInt(moment().format('Y'), 10),
    setYear = () => {},
    month = parseInt(moment().format('M'), 10),
    setMonth = () => {},
    startYear = parseInt(moment().format('Y'), 10),
    endYear = parseInt(moment().format('Y'), 10),
    startMonth = parseInt(moment().format('M'), 10),
    endMonth = parseInt(moment().format('M'), 10),
}) => {
    const getMonth = () => strings.monthNames.map((monthName, i) => ({
        label: monthName,
        value: i + 1,
    }));

    return (
        <div className="calendar-date-picker">
            <FormGroup
                id="year"
                label={strings.year}
                input={
                    <Select
                        id="month"
                        value={month}
                        setValue={setMonth}
                        options={
                            getMonth().filter(
                                month =>
                                    startYear === year && endYear === year ? month.value >= startMonth && month.value <= endMonth :
                                    startYear === year ? month.value >= startMonth :
                                    endYear === year ? month.value <= endMonth :
                                    true
                            ).map(month => ({
                                value: month.value,
                                label: month.label
                            }))
                        }
                        withNotSelected={true}
                    />
                }
            />
            <FormGroup
                id="month"
                label={strings.year}
                input={
                    <Select
                        id="year"
                        value={year}
                        setValue={setYear}
                        options={[...Array(endYear - startYear + 1).keys()].map(year => ({
                            value: year + startYear,
                            label: year + startYear
                        }))}
                        withNotSelected={true}
                    />
                }
                classes="ml-2"
            />
        </div>
    );
};

export default CalendarDatePicker;
