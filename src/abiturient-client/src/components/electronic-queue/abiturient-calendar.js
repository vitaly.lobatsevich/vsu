import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';

import client from '../../client';
import strings from '../../strings';
import {selectDownloaded} from '../../redux/slices/downloaded-slice';
import {selectAdmission} from '../../redux/slices/admission-slice';
import {selectAdmissionDownloaded} from '../../redux/slices/admission-downloaded-slice';
import CommonCalendar from './common-calendar';
import CalendarFilters from './calendar-filters';
import AbiturientCalendarArea from './abiturient-calendar-area';
import CalendarCurrentTime from './calendar-current-time';

const AbiturientCalendar = () => {
    const {faculties, educationForms, specialties} = useSelector(selectDownloaded);
    const admission = useSelector(selectAdmission);
    const isAdmissionDownloaded = useSelector(selectAdmissionDownloaded);

    const [facultyId, setFacultyId] = useState(1);
    const [educationFormId, setEducationFormId] = useState(1);
    const [specialtyId, setSpecialtyId] = useState(0);
    const [isHasAccess, setIsHasAccess] = useState(false);
    const [isCheckingAccess, setIsCheckingAccess] = useState(false);

    useEffect(() => {
        setIsCheckingAccess(true);
        client.get('/api/electronic-queue/is-has-access')
            .then(() => setIsHasAccess(true))
            .catch(error => {
                if (error.response) {
                    switch (error.response.status) {
                        case 403:
                            setIsHasAccess(false);
                            break;
                        default:
                            console.log(error.response);
                    }
                }
            })
            .finally(() => setIsCheckingAccess(false));
    }, []);

    const isLoading = () => !faculties || !educationForms || !specialties || isCheckingAccess || !isAdmissionDownloaded;

    const isNoDesks = () => !admission || !admission.desks || !(admission.desks.findIndex(
        desk => desk.specialties.findIndex(specialty => specialty.id === specialtyId) !== -1
    ) !== -1);

    return (
        <CommonCalendar
            isLoading={isLoading()}
            admission={admission}
            isHasAccess={isHasAccess}
            filters={
                <div>
                    <div className="row mt-3">
                        <div className="col">
                            <h5>
                                {strings.pickSpecialty}
                            </h5>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <CalendarFilters
                                facultyId={facultyId}
                                setFacultyId={setFacultyId}
                                educationFormId={educationFormId}
                                setEducationFormId={setEducationFormId}
                                specialtyId={specialtyId}
                                setSpecialtyId={setSpecialtyId}
                            />
                        </div>
                    </div>
                </div>
            }
            isNoDesks={isNoDesks()}
            calendarArea={
                <AbiturientCalendarArea specialtyId={specialtyId} />
            }
            backLink={
                <Link to="/profile" className="btn btn-outline-primary">
                    {strings.backToProfile}
                </Link>
            }
            currentTimeComponent={<CalendarCurrentTime />}
        />
    );
};

export default AbiturientCalendar;
