import React, {useState, useEffect} from 'react';
import $ from 'jquery';
import moment from 'moment';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import {required} from '../../validators';
import {selectMagistrantAdmission} from '../../redux/slices/magistrant-admission-slice';
import {selectMagistrantBasicInfo, setValue, setIsValid, setError} from '../../redux/slices/magistrant-basic-info-slice';
import Name from '../name';
import Phone from '../phone';
import CommonCalendarArea from './common-calendar-area';
import CommonCalendarCell from './common-calendar-cell';
import CommonCalendarTime from './common-calendar-time';

const MagistrantCalendarArea = () => {
    const dispatch = useDispatch();

    const admission = useSelector(selectMagistrantAdmission);
    const {phone, firstName, lastName, patronymic} = useSelector(selectMagistrantBasicInfo);

    const [isAllValid, setIsAllValid] = useState(false);

    useEffect(
        () => setIsAllValid(phone.isValid &&
            firstName.isValid && lastName.isValid),
        [
            phone.isValid,
            firstName.isValid,
            lastName.isValid
        ]
    );

    const isDayYour = day => false;

    const calendarCell = (key, day, month, selectedDay, setSelectedDay) => (
        <CommonCalendarCell
            key={key}
            admission={admission}
            isDayYour={isDayYour}
            day={day}
            month={month}
            selectedDay={selectedDay}
            setSelectedDay={setSelectedDay}
        />
    );

    const isTimeFree = time => admission.desks
        .findIndex(desk => desk.magistrantQueue.findIndex(e => moment(e.dateTime).isSame(time)) === -1) !== -1;

    const calendarTime = (key, time, setDateTime) => (
        <CommonCalendarTime
            key={key}
            admission={admission}
            isTimeFree={isTimeFree}
            time={time}
            setDateTime={setDateTime}
        />
    );

    const doSetValue = key => value => dispatch(setValue({
        key: key,
        value: value
    }));

    const doSetIsValid = key => isValid => dispatch(setIsValid({
        key: key,
        isValid: isValid
    }));

    const doSetError = key => error => dispatch(setError({
        key: key,
        error: error
    }));

    const getDeskDescriptionByDeskId = deskId => {
        const desk = admission.desks.find(desk => desk.id === deskId);
        return desk ? desk.description : '';
    };

    const handleConfirm = ({data}) => {
        localStorage.setItem('magistrantQueueResultDateTime', data[0].date_time);
        localStorage.setItem('magistrantQueueResultDescription', getDeskDescriptionByDeskId(data[0].desk_id));
        $('#alertModalEndMagistrantQueueRegistration').modal('toggle');
    };

    return (
        <div>
            <div className="row mb-3">
                <div className="col-12">
                    <h5 align="center">
                        {strings.magistrantRegistrationData}:
                    </h5>
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-3 col-lg-4" />
                <div className="col-12 col-md-6 col-lg-4">
                    <form>
                        <Phone
                            id="phone"
                            label={strings.phone + '*'}
                            phone={phone.value}
                            setPhone={doSetValue('phone')}
                            isValid={phone.isValid}
                            setIsValid={doSetIsValid('phone')}
                        />
                        <Name
                            id="lastName"
                            label={strings.lastName + '*'}
                            value={lastName.value}
                            setValue={doSetValue('lastName')}
                            validationError={lastName.error}
                            setValidationError={doSetError('lastName')}
                            isValid={lastName.isValid}
                            setIsValid={doSetIsValid('lastName')}
                            validators={[required]}
                        />
                        <Name
                            id="firstName"
                            label={strings.firstName + '*'}
                            value={firstName.value}
                            setValue={doSetValue('firstName')}
                            validationError={firstName.error}
                            setValidationError={doSetError('firstName')}
                            isValid={firstName.isValid}
                            setIsValid={doSetIsValid('firstName')}
                            validators={[required]}
                        />
                        <Name
                            id="patronymic"
                            label={
                                <div>
                                    {strings.patronymic}
                                    <small className="text-muted"> - {strings.ifAvailable}</small>
                                </div>
                            }
                            value={patronymic.value}
                            setValue={doSetValue('patronymic')}
                            isValid={patronymic.isValid}
                            setIsValid={doSetIsValid('patronymic')}
                        />
                    </form>
                </div>
                <div className="col-12 col-md-3 col-lg-4" />
            </div>
            {isAllValid &&
            <CommonCalendarArea
                admission={admission}
                confirmUrl="/api/electronic-queue/magistrants"
                confirmData={{
                    phone: phone.value,
                    firstName: firstName.value,
                    lastName: lastName.value,
                    patronymic: patronymic.value,
                }}
                calendarCell={calendarCell}
                calendarTime={calendarTime}
                onConfirm={handleConfirm}
            />
            }
        </div>
    );
};

export default MagistrantCalendarArea;
