import React from 'react';
import moment from 'moment';
import {useSelector} from 'react-redux';

import {selectAdmission} from '../../redux/slices/admission-slice';
import {selectDbUser} from '../../redux/slices/db-user-slice';
import CommonCalendarArea from './common-calendar-area';
import CommonCalendarCell from './common-calendar-cell';
import CommonCalendarTime from './common-calendar-time';

const AbiturientCalendarArea = ({specialtyId = 0}) => {
    const admission = useSelector(selectAdmission);
    const {queue} = useSelector(selectDbUser);

    const isDayYour = day => admission && queue.findIndex(
        e => e.admissionId === admission.id &&
            moment(e.dateTime).format('DD.MM.YYYY') === day.format('DD.MM.YYYY')
    ) !== -1;

    const calendarCell = (key, day, month, selectedDay, setSelectedDay) => (
        <CommonCalendarCell
            key={key}
            admission={admission}
            isDayYour={isDayYour}
            day={day}
            month={month}
            selectedDay={selectedDay}
            setSelectedDay={setSelectedDay}
        />
    );

    const getCurrent = () => queue.find(e => e.admissionId === admission.id);

    const isTimeYour = time => admission && getCurrent() && moment(getCurrent().dateTime).isSame(time);

    const isTimeFree = time => admission.desks
        .filter(desk => desk.specialties.findIndex(specialty => specialty.id === specialtyId) !== -1)
        .findIndex(desk => desk.queue.findIndex(e => moment(e.dateTime).isSame(time)) === -1) !== -1;

    const calendarTime = (key, time, setDateTime) => (
        <CommonCalendarTime
            key={key}
            admission={admission}
            isTimeYour={isTimeYour}
            isTimeFree={isTimeFree}
            time={time}
            setDateTime={setDateTime}
        />
    );

    const isSignUp = () => admission && !!queue.find(item => item.admissionId === admission.id);

    return (
        <CommonCalendarArea
            admission={admission}
            confirmUrl="/api/electronic-queue/me"
            confirmData={{
                specialtyId: specialtyId,
            }}
            calendarCell={calendarCell}
            calendarTime={calendarTime}
            isSignUp={isSignUp}
        />
    );
};

export default AbiturientCalendarArea;
