import React from 'react';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import {selectQueueErrorModalContent} from '../../redux/slices/queue-error-modal-content-slice';
import Spinner from '../spinner';
import AlertModal from '../alert-modal';

const CommonCalendar = ({
    isLoading = false,
    admission,
    isHasAccess = true,
    filters = <div />,
    isNoDesks = true,
    calendarArea = <div />,
    backLink = <div />,
    currentTimeComponent = <div />,
}) => {
    const queueErrorModalContent = useSelector(selectQueueErrorModalContent);

    return (
        <div>
            <AlertModal
                id="QueueError"
                content={queueErrorModalContent}
            />
            <div className="row">
                <div className="col">
                    <h3>
                        {strings.recording}
                    </h3>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col">
                    {backLink}
                </div>
            </div>
            {isLoading ?
            <div className="text-center">
                <Spinner />
            </div> :
            !admission ?
            <p>
                {strings.noAdmission}
            </p> :
            <div>
                {currentTimeComponent}
                {isHasAccess ?
                <div>
                    {filters}
                    <div className="row mt-3">
                        <div className="col">
                            {isNoDesks ?
                            <p>
                                {strings.noDesks}
                            </p> :
                            <div className="row">
                                <div className="col-12 mt-3">
                                    <div className="card">
                                        <div className="card-body">
                                            {calendarArea}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div> :
                <p className="mt-3">
                    {strings.notAvaliable}
                </p>
                }
            </div>
            }
        </div>
    );
};

export default CommonCalendar;
