import React, {useContext, useState} from 'react';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
import $ from 'jquery';

import strings from '../../strings';
import client from '../../client';
import {AuthContext, AppContext} from '../../contexts';
import {selectAdmissionDownloaded} from '../../redux/slices/admission-downloaded-slice';
import ConfirmModal from '../confirm-modal';
import Spinner from '../spinner';
import CommonCurrentTime from './common-current-time';

const CalendarCurrentTime = ({isCancelShown = true, isChangeShown = false}) => {
    const admissionDownloaded = useSelector(selectAdmissionDownloaded);
    const {getMe} = useContext(AuthContext);
    const {getAdmission, getCurrentTime, isTimeSelected} = useContext(AppContext);

    const [isCancelLoading, setIsCancelLoading] = useState(false);

    const handleCancelClick = event => {
        event.preventDefault();
        $('#confirmModalCancelRegistration').modal('toggle');
    };

    const handleConfirmClick = () => {
        setIsCancelLoading(true);
        client.delete('/api/electronic-queue/me').then(() => {
            getAdmission();
            getMe(
                () => {},
                () => {},
                () => setIsCancelLoading(false)
            );
        });
    };

    return (
        <div>
            <ConfirmModal
                id="CancelRegistration"
                content={strings.cancelRecording}
                onConfirm={handleConfirmClick}
            />
            {isTimeSelected() &&
            <div>
                {(isCancelLoading || !admissionDownloaded) ?
                <div className="mt-3">
                    <Spinner />
                </div> :
                <CommonCurrentTime
                    currentTime={getCurrentTime().dateTime}
                    description={getCurrentTime().desk.description}
                >
                    <div>
                        <hr />
                        {isCancelShown &&
                        <a href="/cancel" onClick={handleCancelClick}>
                            {strings.cancel}
                        </a>
                        }
                        {isChangeShown &&
                        <Link to="/electronic-queue">
                            {strings.change}
                        </Link>
                        }
                    </div>
                </CommonCurrentTime>
                }
            </div>
            }
        </div>
    );
};

export default CalendarCurrentTime;
