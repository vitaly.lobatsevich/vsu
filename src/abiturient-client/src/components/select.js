import React from 'react';

import strings from '../strings';

const Select = ({
    id = '',
    value = 0,
    setValue = () => {},
    options = [],
    withNotSelected = false,
    classes = '',
}) => {
    const handleChange = ({target}) => setValue(parseInt(target.value, 10));

    return (
        <select
            id={id}
            value={value}
            onChange={handleChange}
            className={`custom-select ${classes}`}
        >
            {!withNotSelected &&
            <option value="0">
                {strings.notSelected}
            </option>
            }
            {options.map(option =>
            <option key={option.value} value={option.value}>
                {option.label}
            </option>
            )}
        </select>
    );
};

export default Select;
