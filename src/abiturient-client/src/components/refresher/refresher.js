import React, {useState} from 'react';
import $ from 'jquery';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import {selectRefreshError} from '../../redux/slices/refresh-error-slice';
import Layout from '../layout';
import ConfirmModal from '../confirm-modal';
import AlertModal from '../alert-modal';
import FormGroup from '../form-group';
import RefresherList from './refresher-list';
import {items} from './refresher-list';

const Refresher = () => {
    const refreshError = useSelector(selectRefreshError);

    const [search, setSearch] = useState('');

    const handleRefreshAllClick = () => $('#confirmModalRefreshAll').modal('toggle');

    const handleRefreshAllConfirm = () => items.forEach(item => $(`#${item.path}Refresh`).trigger('click'));

    return (
        <Layout title={strings.titles.refresher}>
            <ConfirmModal
                id="RefreshAll"
                content={strings.refreshAllConfirmQuestion}
                onConfirm={handleRefreshAllConfirm}
            />
            <AlertModal
                id="RefreshErrors"
                content={refreshError}
            />
            <div className="container">
                <div className="row mb-3">
                    <div className="col">
                        <div className="card">
                            <div className="card-body">
                                <div className="form-inline">
                                    <FormGroup
                                        id="search"
                                        label={strings.search}
                                        input={
                                            <input 
                                                type="text" 
                                                id="search"
                                                value={search}
                                                onChange={({target}) => setSearch(target.value)}
                                                className="form-control ml-1"
                                            />
                                        }
                                        classes="mr-3"
                                    />
                                    <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={handleRefreshAllClick}
                                    >
                                        {strings.refreshAll}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <RefresherList search={search} />
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Refresher;
