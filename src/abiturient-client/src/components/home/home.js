import React from 'react';
import {Link} from 'react-router-dom';

import strings from '../../strings';
import Layout from '../layout';
import RegistrationForm from './registration-form';

const Home = () => (
    <Layout>
        <div className="container">
            <div className="row">
                <div className="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 mb-3">
                    <div className="card">
                        <div className="card-body p-5">
                            <h2 align="justify">{strings.homeTitle1}</h2>
                            <p align="justify">{strings.homeText1}</p>
                            <h4 align="justify">{strings.homeTitle2}</h4>
                            <p align="justify">{strings.homeText2}</p>
                            <h4 align="justify">{strings.homeTitle3}</h4>
                            <p align="justify">{strings.homeText3}</p>
                            <h4 align="justify">{strings.homeTitle4}</h4>
                            <p align="justify">{strings.homeText4}</p>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                    <div className="row">
                        <div className="col-12 mb-3">
                            <div className="card">
                                <div className="card-body p-5">
                                    <RegistrationForm />
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body p-4">
                                    <p align="left" className="card-text">
                                        {strings.magistrantRegistrationText}
                                        &nbsp;
                                        <Link to="/magistrant-electronic-queue">
                                            {strings.magistrantRegistrationLink}
                                        </Link>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
);

export default Home;
