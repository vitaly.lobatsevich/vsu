import React, {useState} from 'react';
import $ from 'jquery';
import {Link, useHistory} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import {validate as validateValue} from '../../validators';
import {setValue, selectRegistrationData} from '../../redux/slices/registration-data-slice';
import Email, {emailValidators} from '../email';
import Password, {passwordValidators} from '../password';
import ConfirmedPassword, {validate as validateConfirmedPassword} from '../confirmed-password';
import Alert from '../alert';
import ConfirmModal from '../confirm-modal';

const RegistrationForm = () => {
    const registrationData = useSelector(selectRegistrationData);
    const dispatch = useDispatch();
    const history = useHistory();

    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [confirmedPasswordError, setConfirmedPasswordError] = useState('');
    const [isEmailValid, setIsEmailValid] = useState(false);
    const [isPasswordValid, setIsPasswordValid] = useState(false);
    const [isConfirmedPasswordValid, setIsConfirmedPasswordValid] = useState(false);
    const [isAgree, setIsAgree] = useState(false);
    const [isAgreeError, setIsAgreeError] = useState('');

    const clearValidation = () => {
        setEmailError('');
        setPasswordError('');
        setConfirmedPasswordError('');

        setIsEmailValid(false);
        setIsPasswordValid(false);
        setIsConfirmedPasswordValid(false);

        setIsAgreeError('');
    };

    const validateIsAgree = () => {
        if (!isAgree) {
            setIsAgreeError(strings.shouldBeAgree);
            return false;
        }
        return true;
    };

    const validate = () => {
        clearValidation();

        const isEmailValid = validateValue(registrationData.email, setEmailError, setIsEmailValid, emailValidators);
        const isPasswordValid = validateValue(
            registrationData.password,
            setPasswordError,
            setIsPasswordValid,
            passwordValidators
        );
        const isConfirmedPasswordValid = validateConfirmedPassword(
            registrationData.confirmedPassword,
            setConfirmedPasswordError,
            setIsConfirmedPasswordValid,
            registrationData.password,
            setPasswordError,
            setIsPasswordValid
        );
        const isAgreeValid = validateIsAgree();

        return isEmailValid && isPasswordValid && isConfirmedPasswordValid && isAgreeValid;
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            client.post('/api/email/check', {
                email: registrationData.email,
            }).then(({data}) => {
                clearValidation();
                if (data)
                    history.push('/registration/personal-info');
                else
                    setEmailError(strings.duplicateEmail);
            });
        }
    };

    const handleIsAgreeClick = event => {
        event.preventDefault();
        $('#confirmModalIsAgree').modal('toggle');
    };

    const handleIsAgreeConfirm = () => setIsAgree(true);

    const handleIsAgreeCancel = () => setIsAgree(false);

    return (
        <form onSubmit={handleSubmit}>
            <ConfirmModal
                id="IsAgree"
                content={
                    <div>
                        <p align="justify">
                            {strings.termsOfUse1}
                        </p>
                        <p align="justify">
                            {strings.termsOfUse2}
                        </p>
                    </div>
                }
                onConfirm={handleIsAgreeConfirm}
                onCancel={handleIsAgreeCancel}
                header={strings.confirmTermsOfUseHeader}
                confirmText={strings.confirmTermOfUseConfirm}
                cancelText={strings.confirmTermOfUseCancel}
            />
            <Email
                value={registrationData.email}
                setValue={value => dispatch(setValue({key: 'email', value: value}))}
                validationError={emailError}
                setValidationError={setEmailError}
                isValid={isEmailValid}
                setIsValid={setIsEmailValid}
            />
            <Password
                id="password"
                label={strings.password}
                value={registrationData.password}
                setValue={value => dispatch(setValue({key: 'password', value: value}))}
                validationError={passwordError}
                setValidationError={setPasswordError}
                isValid={isPasswordValid}
                setIsValid={setIsPasswordValid}
            />
            <ConfirmedPassword
                id="confirmedPassword"
                label={strings.confirmPassword}
                value={registrationData.confirmedPassword}
                setValue={value => dispatch(setValue({key: 'confirmedPassword', value: value}))}
                validationError={confirmedPasswordError}
                setValidationError={setConfirmedPasswordError}
                isValid={isConfirmedPasswordValid}
                setIsValid={setIsConfirmedPasswordValid}
                password={registrationData.password}
                setPasswordValidationError={setPasswordError}
                setIsPasswordValid={setIsPasswordValid}
            />
            <div className="form-group mt-4">
                <div className="custom-control custom-checkbox">
                    <input
                        id="isAgree"
                        type="checkbox"
                        checked={isAgree}
                        onChange={() => {}}
                        onClick={handleIsAgreeClick}
                        className="custom-control-input"
                    />
                    <label
                        htmlFor="isAgree"
                        className="custom-control-label small"
                    >
                        {strings.isAgree}*
                    </label>
                </div>
                <Alert
                    classes="alert-danger mt-3"
                    content={isAgreeError}
                />
            </div>
            <input
                type="submit"
                value={strings.register}
                className="btn btn-primary btn-block btn-lg mb-2 mt-4"
            />
            {strings.alreadyHaveAnAccount} <Link to="/login">{strings.logIn}</Link>
        </form>
    );
};

export default RegistrationForm;
