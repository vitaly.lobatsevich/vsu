import React from 'react';
import {useSelector} from 'react-redux';

import strings from '../strings';
import {selectConstants} from '../redux/slices/constants-slice';
import formatter from '../formatter';
import {isBelarus} from '../validators';
import Text from './text';

export const validateSeries = (
    value = '',
    setValidationError = () => {},
    setIsValid = () => {},
    documentTypeId = 1
) => {
    setValidationError('');
    setIsValid(false);

    if (documentTypeId !== 3) {
        if (value === '') {
            setValidationError(strings.required);
            return false;
        }
        if (value.length < 2) {
            setValidationError(strings.seriesFormat);
            return false;
        }
    }

    setIsValid(true);
    return true;
};

export const validateNumber = (
    value = '',
    setValidationError = () => {},
    setIsValid = () => {},
    documentTypeId = 1
) => {
    setValidationError('');
    setIsValid(false);

    if (documentTypeId !== 3) {
        if (value === '') {
            setValidationError(strings.required);
            return false;
        }
        if (value.length < 7) {
            setValidationError(strings.numberFormat);
            return false;
        }
    }

    setIsValid(true);
    return true;
};

const Passport = ({
    citizenship = 1,
    setCitizenship = () => {},
    documentTypeId = 1,
    setDocumentTypeId = () => {},
    series = '',
    setSeries = () => {},
    number = '',
    setNumber = () => {},
    seriesError = '',
    setSeriesError = () => {},
    numberError = '',
    setNumberError = () => {},
    isSeriesValid = false,
    setIsSeriesValid = () => {},
    isNumberValid = false,
    setIsNumberValid = () => {},
    afterChange = () => {},
}) => {
    const constants = useSelector(selectConstants);

    const doSetNumber = value => {
        setNumberError('');
        setIsNumberValid(false);

        if (documentTypeId !== 3)
            setNumber(value.replace(/[^0-9]/g, ''));
        else
            setNumber(value.toUpperCase().replace(/[^A-Z0-9-]/g, formatter.translate));

        afterChange();
    };

    const doSetSeries = value => {
        setSeriesError('');
        setIsSeriesValid(false);

        const formattedValue = value.toUpperCase();
        if (documentTypeId !== 3)
            setSeries(formattedValue.replace(/[^A-Z]/g, formatter.translate));
        else
            setSeries(formattedValue.replace(/[^A-Z0-9-]/g, formatter.translate));

        afterChange();
    };

    const doSetDocumentTypeId = value => {
        setDocumentTypeId(value);
        doSetSeries('');
        doSetNumber('');

        afterChange();
    };

    const doSetCitizenship = value => {
        setCitizenship(value);
        value === 1 ? doSetDocumentTypeId(1) : doSetDocumentTypeId(2);

        afterChange();
    };

    const handleCitizenshipChange = ({target}) => {
        doSetCitizenship(parseInt(target.value, 10));
    };

    const handleDocumentTypeChange = ({target}) => {
        doSetDocumentTypeId(parseInt(target.value, 10));
    };

    const handleSeriesChange = ({target}) => doSetSeries(target.value);

    const handleNumberChange = ({target}) => doSetNumber(target.value);

    const handleSeriesBlur = () => {
        if (series !== '')
            validateSeries(series, setSeriesError, setIsSeriesValid, documentTypeId);
    };

    const handleNumberBlur = () => {
        if (number !== '')
            validateNumber(number, setNumberError, setIsNumberValid, documentTypeId);
    };

    const getCountryOption = country => (
        <option key={country.id} value={country.id}>
            {country.name}
        </option>
    );

    const getBelarusOption = country => {
        const belarus = constants.countries.find(country => isBelarus(country));
        return belarus ? getCountryOption(belarus) : '';
    };

    return (
        <div>
            <div className="form-group">
                <label htmlFor="citizenship">
                    {strings.citizenship}
                </label>
                <select
                    value={citizenship}
                    id="citizenship"
                    className="custom-select"
                    onChange={handleCitizenshipChange}
                >
                    {getBelarusOption()}
                    {constants.countries
                        .filter(country => !isBelarus(country))
                        .map(country => getCountryOption(country))}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="documentType">
                    {strings.documentType}
                </label>
                <div id="documentType">
                    {constants.personalDocumentTypes.map(item =>
                    <div key={item.id} className="custom-control custom-radio">
                        <input
                            id={`documentType${item.id}`}
                            type="radio"
                            value={item.id}
                            checked={item.id === documentTypeId}
                            name="documentType"
                            className="custom-control-input"
                            onChange={handleDocumentTypeChange}
                            disabled={(citizenship === 1 && item.id !== 1) || (citizenship !== 1 && item.id === 1)}
                        />
                        <label htmlFor={`documentType${item.id}`} className="custom-control-label">
                            {item.name}
                        </label>
                    </div>
                    )}
                </div>
            </div>
            <Text
                id="series"
                label={
                    <div>
                        {strings.series}
                        {documentTypeId !== 3 ? '*' : <small className="text-muted"> - {strings.ifAvailable}</small>}
                    </div>
                }
                value={series}
                validationError={seriesError}
                isValid={isSeriesValid}
                placeholder={documentTypeId !== 3 ? strings.seriesPlaceholder : ''}
                maxLength={documentTypeId !== 3 ? 2 : 100}
                onChange={handleSeriesChange}
                onBlur={handleSeriesBlur}
                help={strings.seriesHelp}
            />
            <Text
                id="number"
                label={
                    <div>
                        {strings.number}
                        {documentTypeId !== 3 ? '*' : <small className="text-muted"> - {strings.ifAvailable}</small>}
                    </div>
                }
                value={number}
                validationError={numberError}
                isValid={isNumberValid}
                placeholder={documentTypeId !== 3 ? strings.numberPlaceholder : ''}
                maxLength={documentTypeId !== 3 ? 7 : 100}
                onChange={handleNumberChange}
                onBlur={handleNumberBlur}
                help={strings.numberHelp}
            />
        </div>
    );
};

export default Passport;
