import React from 'react';
import {CSSTransition} from 'react-transition-group';

import './alert.scss';

const Alert = ({classes, content}) => {
    return (
        <CSSTransition
            in={content !== ''}
            timeout={300}
            classNames="alert-animation"
            unmountOnExit
        >
            <div className={`alert ${classes}`}>
                {content}
            </div>
        </CSSTransition>
    );
};

export default Alert;
