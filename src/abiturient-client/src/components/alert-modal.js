import React from 'react';

import strings from '../strings';
import Modal from './modal';

const AlertModal = ({
    id = '',
    content = '',
}) => (
    <Modal
        id={`alertModal${id}`}
        header={strings.alert}
        body={content}
        footer={
            <button
                type="button"
                className="btn btn-primary"
                data-dismiss="modal"
            >
                {strings.ok}
            </button>
        }
    />
);

export default AlertModal;
