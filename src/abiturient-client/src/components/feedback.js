import React from 'react';
import {CSSTransition} from 'react-transition-group';

import './feedback.scss';

const Feedback = ({isValid, feedback, isIndependent = false}) => {
    return (
        <CSSTransition
            in={feedback !== ''}
            timeout={300}
            classNames="feedback"
            unmountOnExit
        >
            {!isIndependent ?
            <div className={isValid ? 'valid-feedback' : 'invalid-feedback'}>
                {feedback}
            </div> :
            <div className={isValid ? 'valid' : 'invalid'}>
                {feedback}
            </div>
            }
        </CSSTransition>
    );
};

export default Feedback;
