import React from 'react';

import strings from '../strings';
import FormGroup from './form-group';

const Radio = ({
    id = '',
    name = '',
    value = '',
    setValue = () => {},
    values = [],
    isInline = false,
    afterChange = () => {},
    label = strings.gender,
}) => {
    const handleChange = ({target}) => {
        setValue(parseInt(target.value, 10));

        afterChange();
    };

    return (
        <FormGroup
            id={id}
            label={label}
            input={
                <div id="gender">
                    {values.map(item =>
                    <div
                        key={item.id}
                        className={`custom-control custom-radio ${isInline ? 'custom-control-inline' : ''}`}
                    >
                        <input
                            id={id + item.id}
                            type="radio"
                            value={item.id}
                            checked={item.id === value}
                            name={name}
                            className="custom-control-input"
                            onChange={handleChange}
                        />
                        <label htmlFor={id + item.id} className="custom-control-label">
                            {item.name}
                        </label>
                    </div>
                    )}
                </div>
            }
        />
    );
};

export default Radio;
