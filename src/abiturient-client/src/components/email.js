import React from 'react';

import strings from '../strings';
import {required, email} from '../validators';
import Text from './text';

export const emailValidators = [required, email];

const Email = ({
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {}
}) => {
    return (
        <Text
            id="email"
            label={strings.email}
            value={value}
            setValue={setValue}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            placeholder={strings.emailPlaceholder}
            validators={emailValidators}
        />
    );
};

export default Email;
