import React from 'react';
import ReactAutocomplete from 'react-autocomplete';

import {validate} from '../validators';

const AutocompleteInput = ({
    id = '',
    items = [],
    itemId = 0,
    setItemId = () => {},
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    afterChange = () => {},
}) => {
    const setValueWithId = value => {
        setValue(value);

        const item = items.find(item => item.name === value);
        setItemId(item ? item.id : 0);

        afterChange();
    };

    const handleChange = ({target}) => {
        setValidationError('');
        setIsValid(false);

        setValueWithId(target.value);
    };

    const handleBlur = () => {
        if (itemId === 0)
            setValueWithId('');
        if (value !== '')
            validate(value, setValidationError, setIsValid, validators);
    };

    const formatSearchString = str => str.toLowerCase().replace(/ /g, '');

    return (
        <ReactAutocomplete
            getItemValue={item => item.name}
            items={items}
            renderItem={(item, isHighlighted) =>
                <div key={item.id} style={{
                    background: isHighlighted ? 'rgba(200, 200, 200, .8)' : 'rgba(255, 255, 255, .8)',
                }}>
                    {item.name}
                </div>
            }
            inputProps={{
                id: id,
                className: `form-control ${isValid ? 'is-valid' : ''} ${validationError !== '' ? 'is-invalid' : ''}`,
                onBlur: handleBlur,
            }}
            menuStyle={{
                borderRadius: '10px',
                boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                background: 'rgba(255, 255, 255, .8)',
                position: 'fixed',
                overflow: 'auto',
                maxHeight: '35%',
                maxWidth: '300px',
                zIndex: '100',
            }}
            shouldItemRender={(item, value) => formatSearchString(item.name).indexOf(formatSearchString(value)) > -1}
            value={value}
            onChange={handleChange}
            onSelect={setValueWithId}
            wrapperStyle={{
                display: 'block',
            }}
        />
    );
};

export default AutocompleteInput;
