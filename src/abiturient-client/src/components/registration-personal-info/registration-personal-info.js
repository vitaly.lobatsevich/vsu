import React from 'react';

import RegistrationPersonalInfoForm from "./registration-personal-info-form";
import RegistrationLayout from "../registration-layout";

const RegistrationPersonalInfo = () => (
    <RegistrationLayout form={<RegistrationPersonalInfoForm />} />
);

export default RegistrationPersonalInfo;
