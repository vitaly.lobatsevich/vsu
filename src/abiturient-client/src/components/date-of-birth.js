import React from 'react';

import Date from './date';
import {required, dateOfBirth} from '../validators';
import strings from '../strings';

export const dateOfBirthValidators = [required, dateOfBirth];

const DateOfBirth = ({
    date = null,
    setDate = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    afterChange = () => {},
}) => {
    return (
        <Date
            id="dateOfBirth"
            label={strings.dateOfBirth + '*'}
            date={date}
            setDate={setDate}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            validators={dateOfBirthValidators}
            afterChange={afterChange}
        />
    );
};

export default DateOfBirth;
