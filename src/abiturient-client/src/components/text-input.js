import React, {useEffect} from 'react';
import {validate} from '../validators';
import $ from 'jquery';

import './text-input.scss';

const TextInput = ({
    id = '',
    type = 'text',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    placeholder = '',
    validators = [],
    autoFocus = false,
    maxLength = 100,
    help = '',
    onChange = null,
    onBlur = null,
    afterChange = () => {},
    afterBlur = () => {},
}) => {
    const setFormattedValue = value => setValue(value.replace(/\s+/g, ' '));

    useEffect(() => {
        $(() => $('[data-toggle="tooltip"]').tooltip());
        $(() => $('[data-toggle="popover"]').popover());
        $('.popover-dismiss').popover({
            trigger: 'focus'
        });
    }, []);

    const handleChange = ({target}) => {
        setValidationError('');
        setIsValid(false);

        setFormattedValue(target.value);

        afterChange();
    };

    const handleBlur = () => {
        const trimmed = value.trim();
        setValue(trimmed);
        if (trimmed !== '')
            validate(value, setValidationError, setIsValid, validators);

        afterBlur();
    };

    return (
        <div className="input-group">
            <input
                type={type}
                value={value}
                onChange={onChange ? onChange : handleChange}
                id={id}
                className={`form-control 
                    ${validationError !== '' ? 'is-invalid' : ''} 
                    ${isValid ? 'is-valid' : ''}
                    ${help !== '' ? 'form-control-help' : ''}`}
                onBlur={onBlur ? onBlur : handleBlur}
                placeholder={placeholder}
                autoFocus={autoFocus}
                maxLength={maxLength}
            />
            {help !== '' &&
            <div className="input-group-append">
                <button
                    type="button"
                    className={`btn form-control-btn-help 
                        ${validationError !== '' ? 'form-control-btn-help-invalid' : ''}
                        ${isValid ? 'form-control-btn-help-valid' : ''}`}
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="bottom"
                    data-content={help}
                    tabIndex="-1"
                >
                    ?
                </button>
            </div>
            }
        </div>
    );
};

export default TextInput;
