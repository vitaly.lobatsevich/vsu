import React from 'react';

import FormGroup from './form-group';
import NameInput from './name-input';

const Name = ({
    id = '',
    label = '',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    lang = 'ru',
    validators = [],
    autoFocus = false,
    help = '',
    afterChange = () => {},
}) => {
    return (
        <FormGroup
            id={id}
            label={label}
            validationError={validationError}
            input = {
                <NameInput
                    id={id}
                    value={value}
                    setValue={setValue}
                    validationError={validationError}
                    setValidationError={setValidationError}
                    isValid={isValid}
                    setIsValid={setIsValid}
                    lang={lang}
                    validators={validators}
                    autoFocus={autoFocus}
                    help={help}
                    afterChange={afterChange}
                />
            }
        />
    );
};

export default Name;
