import React, {useEffect, useState} from 'react';

import strings from '../strings';
import Feedback from './feedback';

const months = [
    {number: 1, name: strings.january},
    {number: 2, name: strings.february},
    {number: 3, name: strings.march},
    {number: 4, name: strings.april},
    {number: 5, name: strings.may},
    {number: 6, name: strings.june},
    {number: 7, name: strings.july},
    {number: 8, name: strings.august},
    {number: 9, name: strings.september},
    {number: 10, name: strings.october},
    {number: 11, name: strings.november},
    {number: 12, name: strings.december}
];

const DateInputSelect = ({
    id = '',
    label = '',
    date = null,
    setDate = () => {},
    minYear = new Date().getFullYear(),
    maxYear = new Date().getFullYear(),
    validationError = '',
    setValidationError = () => {},
    onChange = () => {},
}) => {
    const [day, setDay] = useState(date && date instanceof Date ? date.getDate() : 0);
    const [month, setMonth] = useState(date && date instanceof Date ? date.getMonth() + 1 : 0);
    const [year, setYear] = useState(date && date instanceof Date ? date.getFullYear() : 0);

    useEffect(() => {
        setDay(date && date instanceof Date ? date.getDate() : 0);
        setMonth(date && date instanceof Date ? date.getMonth() + 1 : 0);
        setYear(date && date instanceof Date ? date.getFullYear() : 0);
    }, [date]);

    const getDays = () => year === 0 || month === 0 ? []
        : [...Array(new Date(year, month, 0).getDate()).keys()].map(item => item + 1);

    const handleDayChange = ({target}) => {
        setDay(parseInt(target.value));
    };

    const handleMonthChange = ({target}) => {
        setMonth(parseInt(target.value));
        if (month === 0 || !getDays().includes(day))
            setDay(0);
    };

    const handleYearChange = ({target}) => {
        setYear(parseInt(target.value));
        if (year === 0) {
            setMonth(0);
            setDay(0);
        }
    };

    useEffect(() => {
        day === 0 || month === 0 || year === 0 ? setDate(null) : setDate(new Date(`${year}-${month}-${day}`));
        setValidationError('');
        onChange();
    }, [day, month, year]);

    return (
        <div className="form-group">
            <label htmlFor={id}>
                {label}
            </label>
            <div className="row">
                <div className="col-3" style={{paddingRight: '0'}}>
                    <select
                        id={id}
                        value={day}
                        onChange={handleDayChange}
                        className={`form-control ${validationError !== '' ? 'is-invalid' : ''}`}
                    >
                        <option value="0">-</option>
                        {getDays().map(day =>
                        <option key={day} value={day}>
                            {day}
                        </option>
                        )}
                    </select>
                </div>
                <div className="col-5" style={{paddingRight: '0'}}>
                    <select
                        value={month}
                        onChange={handleMonthChange}
                        className={`form-control ${validationError !== '' ? 'is-invalid' : ''}`}
                    >
                        <option value="0">-</option>
                        {year === 0 ? [] : months.map(month =>
                        <option key={month.number} value={month.number}>
                            {month.name}
                        </option>
                        )}
                    </select>
                </div>
                <div className="col-4">
                    <select
                        value={year}
                        onChange={handleYearChange}
                        className={`form-control ${validationError !== '' ? 'is-invalid' : ''}`}
                    >
                        <option value="0">-</option>
                        {[...Array(maxYear - minYear + 1).keys()].map(year =>
                        <option key={year + minYear} value={year + minYear}>
                            {year + minYear}
                        </option>
                        )}
                    </select>
                </div>
            </div>
            <Feedback
                isValid={false}
                feedback={validationError}
                isIndependent={true}
            />
        </div>
    );
};

export default DateInputSelect;
