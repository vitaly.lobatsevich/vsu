import strings from './strings';

export const validate = (
    value = '',
    setValidationError = () => {},
    setIsValid = () => {},
    validators = []
) => {
    setValidationError('');
    setIsValid(false);

    for (const validator of validators) {
        if (!validator.validate(value)) {
            setValidationError(validator.errorMessage);
            return false;
        }
    }

    setIsValid(true);
    return true;
};

export const required = {
    validate: value => value && value !== '',
    errorMessage: strings.required
};

export const email = {
    validate: value => value.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/),
    errorMessage: strings.invalidEmailFormat
};

export const password = {
    validate: value => value.match(/[A-ZА-Я]/) && value.match(/[a-zа-я]/) && value.match(/[0-9]/) && value.match(/.{8,32}/),
    errorMessage: strings.passwordFormat
};

export const personalIdentificationNumber = {
    validate: value => value === ''
        || (value.match(/^[0-9]{7}[A-Z][0-9]{3}[A-Z]{2}[0-9]$/)
        && value
            .split('')
            .map(item => item.match(/[A-Z]/) ? item.charCodeAt(0) - 55 : parseInt(item))
            .slice(0, -1)
            .map((item, key) => (key + 1) % 3 === 1 ? item * 7 : (key + 1) % 3 === 2 ? item * 3 : item)
            .reduce((a, b) => a + b, 0) % 10 === parseInt(value.slice(-1))),
    errorMessage: strings.invalidIdentificationNumberFormat
};

export const postcode = {
    validate: value => value === '' || value.match(/^2[1-4][0-7][0-9]{3}$/),
    errorMessage: strings.postcodeFormat
};

export const dateOfBirth = {
    validate: date => !date || Math.abs((new Date(Date.now() - date.getTime())).getFullYear() - 1970) >= 16,
    errorMessage: strings.tooYoung
};

export const isBelarus = country => country.name === 'Республика Беларусь';
