const formatter = {
    str: str => str.replace(/\s+/g, ' ').trim(),
    foreignKey: value => value !== 0 ? value : null,
    date: value => value ? value.toDateString() : null,
    translate: symbol => {
        switch (symbol) {
            case 'А': return 'A';
            case 'В': return 'B';
            case 'Е': return 'E';
            case 'К': return 'K';
            case 'М': return 'M';
            case 'Н': return 'H';
            case 'О': return 'O';
            case 'Р': return 'P';
            case 'С': return 'C';
            case 'Т': return 'T';
            case 'Х': return 'X';
            default: return '';
        }
    },
    translateRuToBy: symbol => {
        switch (symbol) {
            case 'И': return 'І';
            case 'и': return 'і';
            case 'Щ': return 'Ў';
            case 'щ': return 'ў';
            case 'Ъ': return '\'';
            case 'ъ': return '\'';
            default: return '';
        }
    },
    capitalize: str => str.length > 0 ? str.charAt(0).toUpperCase() + str.slice(1) : str,
    addLeadingZeroes: (number, length = 2) => {
        let result = number.toString();
        while (result.length < length)
            result = `0${result}`;
        return result;
    },
};

export default formatter;
