import {configureStore} from '@reduxjs/toolkit';
import registrationDataReducer from './slices/registration-data-slice';
import constantsReducer from './slices/constants-slice';
import downloadedReducer from './slices/downloaded-slice';
import userReducer from './slices/user-slice';
import dbUserReducer from './slices/db-user-slice';
import admissionReducer from './slices/admission-slice';
import admissionDownloadedReducer from './slices/admission-downloaded-slice';
import magistrantAdmissionReducer from './slices/magistrant-admission-slice';
import magistrantAdmissionDownloadedReducer from './slices/magistrant-admission-downloaded-slice';
import magistrantBasicInfoReducer from './slices/magistrant-basic-info-slice';
import queueErrorModalContentReducer from './slices/queue-error-modal-content-slice';
import refreshErrorReducer from './slices/refresh-error-slice';

export default configureStore({
    reducer: {
        registrationData: registrationDataReducer,
        constants: constantsReducer,
        downloaded: downloadedReducer,
        user: userReducer,
        dbUser: dbUserReducer,
        admission: admissionReducer,
        admissionDownloaded: admissionDownloadedReducer,
        magistrantAdmission: magistrantAdmissionReducer,
        magistrantAdmissionDownloaded: magistrantAdmissionDownloadedReducer,
        magistrantBasicInfo: magistrantBasicInfoReducer,
        queueErrorModalContent: queueErrorModalContentReducer,
        refreshError: refreshErrorReducer,
    },
    middleware: []
});
