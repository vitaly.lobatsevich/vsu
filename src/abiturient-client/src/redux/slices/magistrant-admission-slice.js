import {createSlice} from '@reduxjs/toolkit';

export const magistrantAdmissionSlice = createSlice({
    name: 'magistrantAdmission',
    initialState: {
        value: null,
    },
    reducers: {
        set: (state, {payload}) => {state.value = payload;},
    }
});

export const {set} = magistrantAdmissionSlice.actions;

export const selectMagistrantAdmission = state => state.magistrantAdmission.value;

export default magistrantAdmissionSlice.reducer;
