import {createSlice} from '@reduxjs/toolkit';

export const magistrantAdmissionDownloadedSlice = createSlice({
    name: 'magistrantAdmissionDownloaded',
    initialState: {
        value: false,
    },
    reducers: {
        set: (state, {payload}) => {state.value = payload;},
    }
});

export const {set} = magistrantAdmissionDownloadedSlice.actions;

export const selectMagistrantAdmissionDownloaded = state => state.magistrantAdmissionDownloaded.value;

export default magistrantAdmissionDownloadedSlice.reducer;
