import {createSlice} from '@reduxjs/toolkit';

export const defaultValue = {
    phone: {
        value: '',
        isValid: false,
    },
    firstName: {
        value: '',
        isValid: false,
        error: '',
    },
    lastName: {
        value: '',
        isValid: false,
        error: '',
    },
    patronymic: {
        value: '',
        isValid: false,
    },
};

export const magistrantBasicInfoSlice = createSlice({
    name: 'magistrantBasicInfo',
    initialState: {
        value: defaultValue,
    },
    reducers: {
        setValue: (state, {payload}) => {
            state.value[payload.key] = {
                ...state.value[payload.key],
                value: payload.value,
            };
        },
        setIsValid: (state, {payload}) => {
            state.value[payload.key] = {
                ...state.value[payload.key],
                isValid: payload.isValid,
            };
        },
        setError: (state, {payload}) => {
            state.value[payload.key] = {
                ...state.value[payload.key],
                error: payload.error,
            };
        },
    }
});

export const {setValue, setIsValid, setError} = magistrantBasicInfoSlice.actions;

export const selectMagistrantBasicInfo = state => state.magistrantBasicInfo.value;

export default magistrantBasicInfoSlice.reducer;
