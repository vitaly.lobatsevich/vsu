import {createSlice} from '@reduxjs/toolkit';

export const defaultRegistrationData = {
    email: '',
    password: '',
    confirmedPassword: '',
    firstName: '',
    lastName: '',
    patronymic: '',
    genderId: 1,
    dateOfBirth: null,
    citizenship: 1,
    personalDocumentTypeId: 1,
    personalDocumentSeries: '',
    personalDocumentNumber: '',
};

export const registrationDataSlice = createSlice({
    name: 'registrationData',
    initialState: {
        value: defaultRegistrationData
    },
    reducers: {
        set: (state, {payload}) => {
            state.value = payload;
        },
        setValue: (state, {payload}) => {
            state.value[payload.key] = payload.value;
        },
    },
});

export const {set, setValue} = registrationDataSlice.actions;

export const selectRegistrationData = state => state.registrationData.value;

export default registrationDataSlice.reducer;
