import {createSlice} from '@reduxjs/toolkit';
import {defaultConstants} from './constants-slice';

export const defaultDownloaded = {};
Object.keys(defaultConstants).forEach(key => defaultDownloaded[key] = false);

export const downloadedSlice = createSlice({
    name: 'downloaded',
    initialState: {
        value: defaultDownloaded
    },
    reducers: {
        setDownloaded: (state, {payload}) => {
            state.value[payload.key] = true;
        },
    },
});

export const {setDownloaded} = downloadedSlice.actions;

export const selectDownloaded = state => state.downloaded.value;

export default downloadedSlice.reducer;
